import {OnInit} from "@angular/core";

/**
 * The base component.
 *
 * @author JavaSaBr
 */
export abstract class BaseComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }
}
